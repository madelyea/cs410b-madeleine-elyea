owner: public(address)

@public
def __init__():
    self.owner = msg.sender

@public
def v_cashOut():
    selfdestruct(self.owner)

@public
@constant
def v_getBalance() -> wei_value:
    return self.balance

@public
@payable
def __default__():
    pass 
