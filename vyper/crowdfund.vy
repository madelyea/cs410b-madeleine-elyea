#received help from https://vyper.readthedocs.io/en/v0.1.0-beta.13/vyper-by-example.html#crowdfund

struct Contributer :
  userAddress: address
  contribution: wei_value

owner: public(address)
target: public(wei_value)  #target fundraising value
endTime: public(timestamp) #time that fundraiser ends
timeLimit: public(timedelta)
contributers: map(int128, Contributer)
nextContributerIndex: int128
nextRefundIndex: int128

# Setup global variables
@public
def __init__(_owner: address, _target: wei_value, _timeLimit: timedelta):
    self.owner = _owner
    self.target = _target
    self.endTime = block.timestamp + _timeLimit
    self.nextContributerIndex = 0
    self.nextRefundIndex = 0

@public
@payable
def contribute():
    assert block.timestamp < self.endTime

    index: int128 = self.nextContributerIndex

    self.contributers[index] = Contributer({userAddress: msg.sender, contribution: msg.value})
    self.nextContributerIndex = index + 1

@public
def collect():
    assert self.balance >= self.target
    assert msg.sender == self.owner
    selfdestruct(self.owner)
    

@public
def refund():
    assert block.timestamp >= self.endTime 
    assert self.balance < self.target

    index: int128 = self.nextRefundIndex

    for i in range(index, index + 30):
        if i >= self.nextContributerIndex:
            self.nextRefundIndex = self.nextContributerIndex
            return

        send(self.contributers[i].userAddress, self.contributers[i].contribution)
        clear(self.contributers[i])

    self.nextRefundIndex = index + 30
    
@public
@constant
def returnBalance() -> wei_value:
    return self.balance
