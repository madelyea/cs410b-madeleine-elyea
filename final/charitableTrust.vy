#Madeleine Elyea
#Beneficiary/Charitable Trustfund
#This is a trust fund that has two beneficiaries: the Trustor's child, and a charity. 
#The creator of this contract is able to set the child's age (in seconds) and also the age at which the funds will be released to them (in seconds).
#If the Trustor passes before the fund-release age, the Trustee will call EndTrust and the funds will be delivered to the beneficiaries. 


# This trust has two beneficiaries: one child and one charity
trustee: public(address)

beneficiary: public(address)
beneficiaryAge: public(uint256)
fundReleaseAge: public(uint256)
charity: public(address)
trustStart: public(timestamp)
trustEnd: public(timestamp)

trustAmt: uint256(wei)
beneficiaryAmt: uint256(wei)
charityAmt: uint256(wei)

# Bool variable turns True if the Trustee calls endTrust
ended: public(bool)

# Create a trustfund with two beneficiaries: one child and one charity. The childs age and fund-release age are required in seconds. 
@public
def __init__(_beneficiaryAge: uint256, _fundReleaseAge: uint256, _beneficiary: address, _charity: address):
    
    self.trustee = msg.sender
    self.beneficiaryAge = _beneficiaryAge
    self.fundReleaseAge = _fundReleaseAge
    self.beneficiary = _beneficiary
    self.charity = _charity
    self.trustStart = block.timestamp
    self.ended = False
 
#Function to add funds to the trust.
@public
@payable
def addToTrust():
    # Check if trust is over.
    assert not self.ended
    
    #only the trustee can add to the trust
    assert msg.sender == self.trustee
    
    #add value to trust amount and disperse to beneficiary and charity
    self.trustAmt += as_unitless_number(msg.value)
    self.beneficiaryAmt += self.trustAmt * 9 / 10
    self.charityAmt += self.trustAmt * 1 / 10
    self.trustAmt = 0
    
    
#Function to withdraw funds. Returns negative message unless it is past the fund-release age of the beneficiary
@public
def withdrawFunds():
    #Check if the trust is ended
    assert not self.ended
    
    #set as ended
    self.ended = True
    
    #check to make sure the requester is the beneficiary
    assert msg.sender == self.beneficiary
    
    #Check if beneficiary is old enough to receive funds
    assert self.beneficiaryAge + (block.timestamp - self.trustStart) > self.fundReleaseAge
    
    # Disperse funds, 0.9 percent to the child, the rest to charity
    send(self.charity, self.charityAmt)
    send(self.beneficiary, self.beneficiaryAmt)
    
#This function can be called by the trustee if the trustor passes before the child's fund-release age. 
@public
def endTrust():

    # Check if this function has already been called
    assert not self.ended
    
    #Check if the Trustee is calling this function
    assert msg.sender == self.trustee
    
    #set as ended
    self.ended = True
    
    #Disperse funds, 0.9 percent to the child, the rest to charity
    send(self.beneficiary, self.beneficiaryAmt)
    send(self.charity, self.charityAmt)

   
@public
@constant
def returnBeneficiaryBalance() -> uint256(wei):
    return self.beneficiaryAmt
    
@public
@constant
def returnCharityBalance() -> uint256(wei):
    return self.charityAmt 

@public
@constant
def returnTrustAmt() -> uint256(wei):
    return self.trustAmt 
